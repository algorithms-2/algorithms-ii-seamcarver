# Algorithms II Assignment 2: SeamCarver
Solution for assignment 2 of the Princeton University Algorithms II course on Coursera (https://coursera.cs.princeton.edu/algs4/assignments/seam/specification.php).

The goal of this assignment was to leverage a shortest path algorithm for directed acyclic graphs to implement the "seam carving" algorithm for content-aware resizing of images. Handles both vertical and horizontal carving of seams based on the RGB "energy" of each pixel relative to its neighbours.

## Results  

Correctness:  34/34 tests passed

Memory:       6/6 tests passed

Timing:       18/17 tests passed

<b>Aggregate score:</b> 101.18%%*
*includes points for bonus timing tests.


## How to use
SeamCarver.java can be used in conjunction with the client classes provided by course staff in the project starter folder found on the specification page linked to above. Provided with a .png image as a command line argument, those clients can use this class to carve seams out of the picture.
<b>NOTE:</b> Depends on the algs4 library used in the course.
