import edu.princeton.cs.algs4.Picture;

import java.util.ArrayList;

public class SeamCarver {
    private int width;
    private int height;
    private int[][] imagePixels;
    private boolean isVertical;

    public SeamCarver(Picture picture) {
        if (picture == null) throw new IllegalArgumentException();
        this.height = picture.height();
        this.width = picture.width();
        this.isVertical = true;
        imagePixels = new int[height][width];
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                imagePixels[row][col] = picture.getRGB(col, row);
            }
        }
    }

    public double energy(int x, int y) {
        if (!validPixel(y, x)) throw new IllegalArgumentException();
        if (borderPixel(y, x)) return 1000;
        return Math.sqrt(gradientEnergy(y, x, true) + gradientEnergy(y, x, false));
    }

    private double gradientEnergy(int x, int y, boolean xAxis) {
        int rgbOne, rgbTwo;
        if (xAxis) {
            rgbOne = imagePixels[x][y - 1];
            rgbTwo = imagePixels[x][y + 1];
        }
        else {
            rgbOne = imagePixels[x - 1][y];
            rgbTwo = imagePixels[x + 1][y];
        }

        int rOne = (rgbOne >> 16) & 0xFF;
        int gOne = (rgbOne >> 8) & 0xFF;
        int bOne = (rgbOne >> 0) & 0xFF;

        int rTwo = (rgbTwo >> 16) & 0xFF;
        int gTwo = (rgbTwo >> 8) & 0xFF;
        int bTwo = (rgbTwo >> 0) & 0xFF;

        int rDiff = rOne - rTwo;
        int gDiff = gOne - gTwo;
        int bDiff = bOne - bTwo;

        return rDiff * rDiff + gDiff * gDiff + bDiff * bDiff;
    }

    private boolean borderPixel(int row, int col) {
        return row == 0 || col == 0 ||
                row >= height() - 1 || col >= width() - 1;
    }

    private boolean validPixel(int row, int col) {
        return row >= 0 && col >= 0
                && row < height() && col < width();
    }


    public Picture picture() {
        Picture pic = new Picture(width, height);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                pic.setRGB(i, j, imagePixels[j][i]);
            }
        }
        return pic;
    }

    public int width() {
        return isVertical ? this.width : this.height;
    }

    public int height() {
        return isVertical ? this.height : this.width;
    }

    public int[] findHorizontalSeam() {
        this.isVertical = false;
        this.imagePixels = transpose(this.imagePixels);
        int[] seam = findVerticalSeam();
        this.isVertical = true;
        this.imagePixels = transpose(this.imagePixels);
        return seam;
    }

    public int[] findVerticalSeam() {
        double[][] distToSPRoot = setupDistMatrix();
        // Part 1: calculate distance to Shortest Path Root of every cell below top row.
        double[][] energyMatrix = energyMatrix();
        for (int row = 0; row < height() - 1; row++) {
            for (int col = 0; col < width(); col++) {
                ArrayList<int[]> adj = adj(row, col, true);
                for (int[] cell : adj
                ) {
                    int x = cell[0], y = cell[1];
                    if (distToSPRoot[row][col] + energyMatrix[x][y]
                            < distToSPRoot[x][y]) {
                        distToSPRoot[x][y] = distToSPRoot[row][col]
                                + energyMatrix[x][y];
                    }
                }
            }
        }
        // Part 2: retrace shortest path starting at the column with lowest energy in bottom row.
        int[] seam = new int[height()];
        int rowMinIndex = -1;
        double rowMinDist = Double.POSITIVE_INFINITY;
        int rowNum = height() - 1;
        double[] bottomRow = distToSPRoot[rowNum];
        for (int i = 0; i < bottomRow.length; i++) {
            if (bottomRow[i] < rowMinDist) {
                rowMinDist = bottomRow[i];
                rowMinIndex = i;
            }
        }
        seam[rowNum] = rowMinIndex;

        while (rowNum > 0) {
            rowMinIndex = -1;
            rowMinDist = Double.POSITIVE_INFINITY;
            for (int[] cell : adj(rowNum,
                                  seam[rowNum],
                                  false)
            ) {
                int x = cell[0], y = cell[1];
                if (distToSPRoot[x][y] < rowMinDist) {
                    rowMinDist = distToSPRoot[x][y];
                    rowMinIndex = y;
                }
            }
            seam[--rowNum] = rowMinIndex;
        }
        return seam;
    }

    private double[][] energyMatrix() {
        double[][] energyMatrix = new double[height()][width()];
        for (int row = 0; row < height(); row++) {
            for (int col = 0; col < width(); col++) {
                energyMatrix[row][col] = energy(col, row);
            }
        }
        return energyMatrix;
    }

    private double[][] setupDistMatrix() {
        double[][] distToSPRoot = new double[height()][width()];
        for (int row = 0; row < height(); row++) {
            for (int col = 0; col < width(); col++) {
                if (row == 0)
                    distToSPRoot[row][col]
                            = 0;
                else distToSPRoot[row][col] = Double.POSITIVE_INFINITY;
            }
        }
        return distToSPRoot;
    }

    private int[][] transpose(int[][] pixels) {
        int n = pixels[0].length;
        int m = pixels.length;
        int[][] transposed = new int[n][m];
        for (int x = 0; x < n; x++) {
            for (int y = 0; y < m; y++) {
                transposed[x][y] = pixels[y][x];
            }
        }
        return transposed;
    }

    private ArrayList<int[]> adj(int x, int y, boolean aimsDown) {
        int xValue = aimsDown ? x + 1 : x - 1;
        int[] yValues = new int[] { y - 1, y, y + 1 };
        ArrayList<int[]> adj = new ArrayList<>();
        for (int i = 0; i < yValues.length; i++) {
            int yValue = yValues[i];
            if (validPixel(xValue, yValue)) {
                adj.add(new int[] { xValue, yValue });
            }
        }
        return adj;
    }

    public void removeHorizontalSeam(int[] seam) {
        if (!validSeam(seam, false)) throw new IllegalArgumentException();
        this.isVertical = false;
        this.imagePixels = transpose(this.imagePixels);
        removeVerticalSeam(seam);
        this.isVertical = true;
        this.imagePixels = transpose(this.imagePixels);
    }

    public void removeVerticalSeam(int[] seam) {
        if (!validSeam(seam, true)) throw new IllegalArgumentException();
        for (int row = 0; row < seam.length; row++) {
            int hole = seam[row];
            for (int col = hole; col < width() - 1; col++) {
                this.imagePixels[row][col] = this.imagePixels[row][col + 1];
            }
        }
        if (isVertical) width--;
        else height--;
    }

    private boolean validSeam(int[] seam, boolean seamIsVertical) {
        int lengthCmp = seamIsVertical ? height() : width();
        int indexCmp = seamIsVertical ? width() : height();
        if (seam == null || seam.length != lengthCmp) return false;
        for (int i = 0; i < lengthCmp; i++) {
            if ((seam[i] < 0) || (seam[i] >= indexCmp))
                return false;
            if (i > 0 && Math.abs(seam[i] - seam[i - 1]) > 1) {
                return false;
            }
        }
        return true;
    }
}